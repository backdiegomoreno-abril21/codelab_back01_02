# README #

* API Products. Entregables:

>>> BACK01 - API REST con integración de métodos GET, POST, PUT, DELETE y PATCH y control de respuestas HTTP.

>>> BACK02 - Integración operaciones CRUD en MongoDB - API REST.

### Autor ###

* Diego Javier Moreno Rusinque
* Version 1.0

### Docker ###

* [Imagen Docker Hub](https://hub.docker.com/repository/docker/diegojavierrusinque/apiproducts)