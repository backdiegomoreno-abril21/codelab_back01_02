FROM openjdk:15-jdk-alpine
COPY target/api-0.0.1.jar api.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/api.jar"]