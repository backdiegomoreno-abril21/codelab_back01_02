package net.techu.controllers;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoOnlyPrecio;
import net.techu.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductosV4Controller {

    @Autowired
    ProductoService productoService;

    /* Get lista de productos */
    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = productoService.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    /* Get producto by nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        List<ProductoMongo> resultado = productoService.findByNombre(nombre);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    /* Get producto by precio */
    @GetMapping(value="/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorPrecio(@PathVariable double minimo,
                                                                        @PathVariable double maximo)
    {
        List<ProductoMongo> resultado = productoService.findByPrecio(minimo, maximo);
        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v4/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoNuevo)
    {
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.toString());
        productoService.save(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* Update producto */
    @PutMapping("/v4/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id,
                                                 @RequestBody ProductoMongo cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Optional<ProductoMongo> productoAModificar = productoService.findById(id);
            if(productoAModificar.isPresent()) {
                productoAModificar.get().nombre = cambios.nombre;
                productoAModificar.get().precio = cambios.precio;
                productoAModificar.get().color = cambios.color;
                productoAModificar.get().clients = cambios.clients;
            }
            productoService.save(productoAModificar.get());
            resultado = new ResponseEntity<String>(productoAModificar.toString(), HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    // Update not idempotent
    @PatchMapping("/v4/productos/{id}")
    public ResponseEntity updatePrecioProducto(@RequestBody ProductoOnlyPrecio productoPrecio,
                                              @PathVariable String id){
        Optional<ProductoMongo> pr = productoService.findById(id);
        if (!pr.isPresent()) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.get().precio = productoPrecio.getPrecio();
        System.out.println(pr.get());
        productoService.save(pr.get());
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    /* Delete id producto */
    @DeleteMapping("/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            productoService.deleteById(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /* Get idClient producto */
    @GetMapping("/v4/productos/{id}/clients")
    public ResponseEntity getProductIdUsers(@PathVariable String id){
        Optional<ProductoMongo> pr = productoService.findById(id);
        if (!pr.isPresent()) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.get().clients != null)
            return ResponseEntity.ok(pr.get().clients);
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
