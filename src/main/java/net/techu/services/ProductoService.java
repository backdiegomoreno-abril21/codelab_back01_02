package net.techu.services;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoOnlyPrecio;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    // READ Collections
    public List<ProductoMongo> findAll() {
        return productoRepository.findAll();
    }

    // READ instance
    public Optional<ProductoMongo> findById(String  id) {
        return productoRepository.findById(id);
    }

    /* Get producto by nombre */
    public List<ProductoMongo> findByNombre(String nombre) {
        return productoRepository.findByNombre(nombre);
    }

    /* Get producto by precio */
    public List<ProductoMongo> findByPrecio(double minimo, double maximo) {
        return productoRepository.findByPrecio(minimo, maximo);
    }

    // CREATE
    public ProductoMongo save(ProductoMongo entity) {
        return productoRepository.save(entity);
    }

    // DELETE
    public boolean deleteById(String index) {
        try {
            productoRepository.deleteById(index);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}