package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("productosdiego")
public class ProductoMongo {

    public String id;
    public String nombre;
    public double precio;
    public String color;
    public List<ClienteMongo> clients;

    public ProductoMongo(String nombre, double precio, String color, List<ClienteMongo> clients) {
        this.nombre = nombre;
        this.precio = precio;
        this.color = color;
        this.clients = clients;
    }

    @Override
    public String toString() {
        return String.format("Producto [id=%s, nombre=%s, precio=%s, color=%s, clients=%s]", id, nombre, precio, color, clients);
    }

}
