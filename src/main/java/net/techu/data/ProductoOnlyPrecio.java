package net.techu.data;

import java.util.List;

public class ProductoOnlyPrecio {

    private String id;
    private double precio;

    public ProductoOnlyPrecio() {
    }

    public ProductoOnlyPrecio(String id, double precio) {
        this.id = id;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "ProductoOnlyPrecio{" +
                "id='" + id + '\'' +
                ", precio=" + precio +
                '}';
    }

}
