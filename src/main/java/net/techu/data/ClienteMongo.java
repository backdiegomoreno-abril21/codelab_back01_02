package net.techu.data;

public class ClienteMongo {

    private String id;

    public ClienteMongo() {
    }

    public ClienteMongo(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ClienteMongo{" +
                "id='" + id + '\'' +
                '}';
    }
}
